default: build-all

build-grammar: grammar.ebnf
	mkdir -p public
	ebnf2railroad --title 'EBNF Grammar' grammar.ebnf -o public/grammar.html

build-index: index.html
	mkdir -p public
	cp index.html public

build-all: build-grammar build-index
